from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from food import views

router = routers.DefaultRouter()
router.register('', views.FoodViewSet, basename='food')

app_name='food'

urlpatterns = [
    path('', include(router.urls)),
]