from django.db import models

# Create your models here.


class Food(models.Model):
    name = models.CharField(max_length=1024)
    amount = models.CharField(max_length=1024)
    carbohydrates = models.IntegerField()

    class Meta:
        db_table = 'lailah_food'
        managed = True

    def __str__(self):
        return self.name
