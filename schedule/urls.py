from django.conf.urls import include
from django.urls import path
from rest_framework import routers

from schedule.views import (CalendarViewSet,
                            EventViewSet,
                            InstanceViewSet,
                            RecurrenceViewSet)


router = routers.DefaultRouter()
router.register('calendar', CalendarViewSet, basename='calendar')
router.register('event', EventViewSet, basename='event')
router.register('instance', InstanceViewSet, basename='instance')
router.register('recurrence', RecurrenceViewSet, basename='recurrence')

app_name = 'schedule'

urlpatterns = [
    path('', include(router.urls)),
]