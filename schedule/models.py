from django.db import models

from core.models import User


class Calendar(models.Model):
    name = models.CharField(max_length=1024)
    owner = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)

    class Meta:
        db_table = 'schedule_calendar'
        managed = True

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=128)
    description = models.CharField(max_length=1024)

    class Meta:
        db_table = 'schedule_event'
        managed = True

    def __str__(self):
        return self.name


class Instance(models.Model):
    event = models.ForeignKey(Event, null=False, blank=False, on_delete=models.PROTECT)
    from_time = models.DateTimeField()
    to_time = models.DateTimeField()
    recurrence = models.ForeignKey('Recurrence', null=True, on_delete=models.PROTECT)

    class Meta:
        db_table = 'schedule_instance'
        managed = True


class Recurrence(models.Model):
    frequency = models.IntegerField()
    from_date = models.DateField()
    to_date = models.DateField()

    class Meta:
        db_table = 'schedule_recurrence'
        managed = True
