from rest_framework import viewsets

from schedule.models import (Calendar,
                             Event,
                             Instance,
                             Recurrence)

from schedule.serializers import (CalendarSerializer,
                                  EventSerializer,
                                  InstanceSerializer,
                                  RecurrenceSerializer)


class CalendarViewSet(viewsets.ModelViewSet):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class InstanceViewSet(viewsets.ModelViewSet):
    queryset = Instance.objects.all()
    serializer_class = InstanceSerializer


class RecurrenceViewSet(viewsets.ModelViewSet):
    queryset = Recurrence.objects.all()
    serializer_class = RecurrenceSerializer
