import ldap
from django_auth_ldap.config import LDAPSearch, LDAPGroupQuery

AUTH_LDAP_SERVER_URI = lailah_config['ldap']['uri']

AUTH_LDAP_BIND_DN = lailah_config['ldap']['bind-dn']
AUTH_LDAP_BIND_PASSWORD = lailah_config['ldap']['bind-pw']
AUTH_LDAP_USER_SEARCH = LDAPSearch(
    lailah_config['ldap']['user-search'],
    ldap.SCOPE_SUBTREE,
    lailah_config['ldap']['user-search-scope'],
)

AUTH_LDAP_USER_ATTR_MAP = {
    'first_name': 'givenName',
    'last_name': 'sn',
    'email': 'mail',
}

AUTH_LDAP_ALWAYS_UPDATE_USER = True

AUTH_LDAP_CACHE_TIMEOUT = 3600

AUTHENTICATION_BACKENDS = (
    'django_auth_ldap.backend.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)
