import os

from celery import app, Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'api.settings')

app = Celery('api')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.conf.beat_schedule = {
    'load_cgm_data': {
        'task': 'load_cgm',
        'schedule': 60.0,
    },
}

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')
