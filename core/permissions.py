from rest_framework import permissions


def is_superuser(request):
    try:
        if request.user.is_superuser:
            return True
    except AttributeError:
        return False


def is_application(request):
    try:
        if request.user.is_application:
            return True
    except AttributeError:
        return False


# User Permissions
class CanListUser(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if is_superuser(request):
            return True
        else:
            return False


class CanRetrieveUser(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if is_superuser(request):
            return True
        else:
            return False


class CanCreateUser(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if is_superuser(request):
            return True
        else:
            return False


class CanUpdateUser(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if is_superuser(request):
            return True
        else:
            return False


class CanDestroyUser(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if is_superuser(request):
            return True
        else:
            return False