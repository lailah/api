from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from core import views

router = routers.DefaultRouter()
router.register('user', views.UserViewSet, basename='user')
router.register('system', views.SystemSettingViewSet, basename='system')


app_name='core'

urlpatterns = [
    path('', include(router.urls)),
]