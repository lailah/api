from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from encrypted_model_fields.fields import EncryptedCharField

from .managers import UserManager


class CGM(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'core_cgm'
        managed = True

    def __str__(self):
        return self.name


class User(AbstractBaseUser, PermissionsMixin):
    is_superuser = models.BooleanField(default=False, editable=True)
    is_diabetic = models.BooleanField(default=False, editable=True)
    is_caregiver = models.BooleanField(default=False, editable=True)
    username = models.CharField(max_length=256, unique=True)
    first_name = models.CharField(max_length=256)
    last_name = models.CharField(max_length=256)
    email = models.EmailField(unique=True)
    cgm = models.ForeignKey(CGM, null=True, blank=True, on_delete=models.PROTECT)
    cgm_username = models.CharField(max_length=256, unique=True)
    cgm_password = EncryptedCharField(max_length=128)
    is_active = models.BooleanField(default=True, editable=True)
    last_login = models.DateField(auto_now=True)
    created = models.DateField(auto_now_add=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    class Meta:
        db_table = 'auth_user'
        managed = True
        verbose_name = 'user'
        verbose_name_plural = 'users'


class SystemSetting(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1024)
    value = models.CharField(max_length=1024)

    class Meta:
        db_table = 'core_systemsetting'
        managed = True

    def __str__(self):
        return self.name

