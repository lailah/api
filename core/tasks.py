import os
import yaml
from api.settings import CONFIG_DIR
from datetime import datetime
from celery import shared_task

from monitor.models import GlucoseMonitor

# Config file(s)
with open(os.path.join(CONFIG_DIR, 'settings.yaml'), 'r') as settings_yaml:
    lailah_config = yaml.safe_load(settings_yaml)


@shared_task(name='load_cgm')
def load_cgm():
    ctime = datetime.now()
    from core.models import CGM, User
    users = User.objects.filter(cgm__gt=0)
    for u in users:
        user = u
        cgm = CGM.objects.get(pk=u.cgm_id)
        if cgm.name == 'Dexcom':
            from pydexcom import Dexcom
            dexcom = Dexcom(u.cgm_username, u.cgm_password, ous=True)  # add ous=True if outside of US
            bg = dexcom.get_current_glucose_reading()
            if isinstance(bg, type(None)):
                print('No Data')
            else:
                obj, created = GlucoseMonitor.objects.get_or_create(
                    user=user,
                    cgm=cgm,
                    blood_glucose=bg.mmol_l,
                    trend=bg.trend,
                    created=bg.time,
                    modified=bg.time
                )
                obj.save()

