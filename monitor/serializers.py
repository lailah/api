from rest_framework import serializers

from monitor.models import (Insulin,
                            Bolus,
                            InjectionSite,
                            GlucoseMonitor)

from schedule.models import (Calendar,
                             Event,
                             Instance)


class InsulinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Insulin
        fields = '__all__'


class BolusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bolus
        fields = '__all__'


class InjectionSiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = InjectionSite
        fields = '__all__'


class GlucoseMonitorSerializer(serializers.ModelSerializer):

    class Meta:
        model = GlucoseMonitor
        fields = '__all__'
