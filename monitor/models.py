from django.db import models
from simple_history.models import HistoricalRecords

from core.models import CGM, User
from schedule.models import Calendar, Event


class Insulin(models.Model):
    brand_name = models.CharField(max_length=128)
    type = models.CharField(max_length=128)

    class Meta:
        db_table = 'lailah_insulin'
        managed = True

    def __str__(self):
        return self.brand_name


class Bolus(models.Model):
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    event = models.ForeignKey(Event, null=False, blank=False, on_delete=models.PROTECT)
    carb_ratio = models.IntegerField()
    sensitivity_factor = models.IntegerField()
    target_glucose = models.IntegerField()
    insulin = models.ForeignKey(Insulin, null=False, blank=False, on_delete=models.PROTECT)
    created = models.DateTimeField(auto_now_add=True, blank=False)
    modified = models.DateTimeField(auto_now_add=True, blank=False)
    history = HistoricalRecords(table_name='lailah_bolushistory')

    class Meta:
        db_table = 'lailah_bolus'
        managed = True


class InjectionSite(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'lailah_injectionsite'
        managed = True

    def __str__(self):
        return self.name


class GlucoseMonitor(models.Model):
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    cgm = models.ForeignKey(CGM, null=True, blank=True, on_delete=models.PROTECT)
    blood_glucose = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    trend = models.IntegerField(null=True, blank=True)
    notes = models.CharField(null=True, blank=True, max_length=2048)
    created = models.DateTimeField(blank=False)
    modified = models.DateTimeField(blank=False)

    class Meta:
        db_table = 'lailah_glucosemonitor'
        managed = True


class InjectionMonitor(models.Model):
    user = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    source = models.IntegerField(default=1)
    calendar = models.ForeignKey(Calendar, null=False, blank=False, on_delete=models.PROTECT)
    event = models.ForeignKey(Event, null=False, blank=False, on_delete=models.PROTECT)
    carbohydrates = models.IntegerField(null=True, blank=True)
    expected_insulin_units = models.DecimalField(null=True, blank=True, max_digits=4, decimal_places=2)
    administered_insulin_units = models.DecimalField(null=True, blank=True, max_digits=4, decimal_places=2)
    administered_by = models.ForeignKey(User, null=True, blank=True, on_delete=models.PROTECT, related_name='administered_by')
    body_part = models.ForeignKey('InjectionSite', null=False, blank=False, on_delete=models.PROTECT)
    insulin_type = models.ForeignKey('Insulin', null=True, blank=True, on_delete=models.PROTECT)
    notes = models.CharField(null=True, blank=True, max_length=2048)
    created = models.DateTimeField(auto_now_add=True, blank=False)
    modified = models.DateTimeField(auto_now_add=True, blank=False)

    class Meta:
        db_table = 'lailah_injectionmonitor'
        managed = True
