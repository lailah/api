from django.conf.urls import url, include
from django.urls import path
from rest_framework import routers

from monitor.views import (InsulinViewSet,
                           BolusViewSet,
                           InjectionSiteViewSet,
                           GlucoseMonitorViewSet)

router = routers.DefaultRouter()
router.register('insulin', InsulinViewSet, basename='insulin')
router.register('bolus', BolusViewSet, basename='bolus')
router.register('injectionsite', InjectionSiteViewSet, basename='injectionsite')
router.register('glucose', GlucoseMonitorViewSet, basename='glucose')

app_name='monitor'

urlpatterns = [
    path('', include(router.urls)),
]