from rest_framework import viewsets

from monitor.models import (Insulin,
                            Bolus,
                            InjectionSite,
                            GlucoseMonitor)

from monitor.serializers import (InsulinSerializer,
                                 BolusSerializer,
                                 InjectionSiteSerializer,
                                 GlucoseMonitorSerializer)


class InsulinViewSet(viewsets.ModelViewSet):
    queryset = Insulin.objects.all()
    serializer_class = InsulinSerializer


class BolusViewSet(viewsets.ModelViewSet):
    queryset = Bolus.objects.all()
    serializer_class = BolusSerializer


class InjectionSiteViewSet(viewsets.ModelViewSet):
    queryset = InjectionSite.objects.all()
    serializer_class = InjectionSiteSerializer


class GlucoseMonitorViewSet(viewsets.ModelViewSet):
    queryset = GlucoseMonitor.objects.all()
    serializer_class = GlucoseMonitorSerializer